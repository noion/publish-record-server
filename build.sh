#!/bin/bash
export GOPATH=`pwd`

echo ${LD_LIBRARY_PATH}

go get github.com/cihub/seelog
go get github.com/streadway/amqp
go get gopkg.in/yaml.v2
go get github.com/pborman/uuid
go get github.com/mattn/go-sqlite3

go build -o publish-record-server main

installdir="/home/publish-record-server"
sbindir="/home/publish-record-server/bin"
configdir="/home/publish-record-server/conf"
systemddir="/home/publish-record-server/systemd"
logdir="/home/publish-record-server/log"

mkdir -p ${sbindir}
mkdir -p ${configdir}
mkdir -p ${systemddir}
mkdir -p ${logdir}

cp ./publish-record-server ${sbindir}
cp ./conf/publish-record-server.config ${configdir}
cp ./conf/seelog.xml ${configdir}
cp ./systemd/publish-record-server.service /etc/systemd/system

systemctl enable publish-record-server.service