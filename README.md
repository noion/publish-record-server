#publish-record-server

### 编译安装
首先需要安装go环境，运行命令 yum install go。如果无法利用yum安装，则可[下载](http://golangtc.com/download)后安装。安装go环境后，运行./build.sh进行编译安装，默认路径为/home/publish-record-server。

### 配置
配置文件格式为YAML

|字段名称|字段说明|
| --------   | :----- |
|LogConfig|日志配置文件路径 |
|SqlUrl | 数据库连接或者sqlite路径 |
| amqp|rabbitmq连接|
|HttpIp| httpAPI监听IP|
|HttpPort | httpAPI监听端口|
|RtmpHost | RTMP流服务器 IP:Port|
|Root | 推流录像文件根目录|
|Duration | 播放时长（单位分钟）|
例子：
```
LogConfig:	  seelog.xml
SqlUrl:			 publish-record-server.db
HttpIp:			192.168.199.182
HttpPort:		8090
Root: 			  /home/ln/LQ/publish-record-server/Root
RtmpHost:	  192.168.199.182:1935
```

###开启服务
安装后运行systemctl start publish-record-server.service开启服务
###重启服务
systemctl restart publish-record-server.service
###停止服务
systemctl stop publish-record-server.service

### rabbitmq队列监听
- 创建任务队列 publish.record.server.task
- 创建任务路由键 publish.record.server.rkey

### 消息格式
####创建推流任务
```javascript
{
	"vhost": "",
	"streamName": "",
	"hub": "",
	"host": ""
}
```
|字段名称|字段说明|
| --------   | :----- |
|vhost| vhost名称 参考[RTMP的URL/Vhost规则](https://github.com/ossrs/srs/wiki/v1_CN_RtmpUrlVhost)|
|streamName| 流名称|
|hub| 直播空间名 |
|host| rtmp地址的host |



