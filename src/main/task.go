package main

import (
	"fmt"
	"sync"
	"time"

	log "github.com/cihub/seelog"
	"golang.org/x/net/context"
)

//接收任务消息，管理推流协程的创建关闭
type TaskManager struct {
	db          DBService
	waitCh      chan StreamInfo
	taskEndCh   chan int
	ctx         context.Context
	cancel      context.CancelFunc
	duration    int //单位分钟
	taskIdCount int //任务id计数

	idTaskMap map[int]*Task
	mtx       sync.Mutex
}

func NewTaskManager(db *DBService, duration int) *TaskManager {
	return &TaskManager{
		db:        *db,
		waitCh:    make(chan StreamInfo, 100),
		duration:  duration,
		taskEndCh: make(chan int),
		idTaskMap: make(map[int]*Task)}
}

//启动
func (me *TaskManager) Start() error {
	ctx, cancel := context.WithCancel(context.Background())
	me.ctx = ctx
	me.cancel = cancel

	go me.waitTaskCycle()
	return nil
}

func (me *TaskManager) PushTask(vhost, hub, stream, host string) {
	id, err := me.db.GetStreamId(vhost, hub, stream)
	if err != nil {
		log.Error("query stream id failed with vhost=", vhost,
			" hub=", hub, " stream_name=", stream)
		return
	}
	si := StreamInfo{Vhost: vhost, Hub: hub, StreamName: stream, Host: host, StreamId: id}
	me.waitCh <- si
}

func (me *TaskManager) pushTask(si StreamInfo) *Task {
	me.mtx.Lock()
	defer me.mtx.Unlock()

	for _, task := range me.idTaskMap {
		if (task.StreamName == si.StreamName) &&
			(task.Hub == si.Hub) &&
			(task.Vhost == si.Vhost) {

			//已经在推流，更新开始时间
			task.updateStartTime(time.Now())
			return nil
		}
	}

	//否则，创建新任务
	var ftMapper FileTimeMapper = me.db
	me.taskIdCount = me.taskIdCount + 1
	task := NewTask(me.duration, si, ftMapper, me.taskIdCount, me.taskEndCh)
	me.idTaskMap[me.taskIdCount] = task

	go task.StartPublish()
	log.Debug("task ", me.taskIdCount, " start publish")
	return task
}

//等待消息协程
func (me *TaskManager) waitTaskCycle() {
	for {
		select {
		case <-me.ctx.Done():
			return
		case id := <-me.taskEndCh:
			me.delTask(id)
		case si := <-me.waitCh:
			me.pushTask(si)
		}
	}
	//结束所有任务协程

	close(me.waitCh)
	log.Debug("quit waitTaskCycle")
}

func (me *TaskManager) delTask(id int) {
	me.mtx.Lock()
	defer me.mtx.Unlock()

	if _, ok := me.idTaskMap[id]; ok {
		delete(me.idTaskMap, id)
	}
}

func (me *TaskManager) Quit() {

}

type FileTimeMapper interface {
	GetFileTime(StreamId, t int) *FileTime
}

//推流任务，一个推流任务是指接收到推流指令后，持续推流durtion时长，中间可能
//需要切换多个推流文件，创建多个Publisher
type Task struct {
	startTime  int
	pub        *Publisher //正在执行的推流
	duration   int        //固定推流时长，单位分钟
	taskId     int        //任务Id
	StreamInfo            //流信息
	ftMapper   FileTimeMapper
	taskEndCh  chan<- int
}

func NewTask(duration int, si StreamInfo, ftmpper FileTimeMapper, taskId int, taskEndCh chan<- int) *Task {
	return &Task{
		duration:   duration,
		StreamInfo: si,
		ftMapper:   ftmpper,
		taskId:     taskId,
		taskEndCh:  taskEndCh}
}

func (me *Task) isExipred() bool {
	now := int(time.Now().Unix() / 1000)
	return now < me.duration+me.startTime
}

func (me *Task) updateStartTime(t time.Time) {
	me.startTime = GetDayTimeSeconds(t)
	log.Debug(PrintTaskId(me),
		fmt.Sprintf("%s %s %s task update startTime %s",
			me.Vhost, me.Hub, me.StreamName, t.String()))
}

func (me *Task) StartPublish() {
	me.startTime = GetDayTimeSeconds(time.Now())

	var ft *FileTime
	var nowTime time.Time
	var nowDT int
	for {
		for i := 0; i < 10; i++ {
			nowTime = time.Now()
			nowDT = GetDayTimeSeconds(nowTime)

			ft = me.ftMapper.GetFileTime(me.StreamId, nowDT)
			if ft == nil {
				log.Info(PrintTaskId(me), "未查询到该时间的映射文件：", nowDT)
				time.Sleep(1 * time.Second)
			} else {
				break
			}
		}

		if ft == nil {
			log.Info(PrintTaskId(me), "未查询到该时间的映射文件, 推流失败")
			break
		}

		restDuration := me.startTime + me.duration*60 - nowDT

		//计算文件需要推流时间偏移
		offset := nowDT - ft.StartDT.Seconds()
		log.Debug("nowDT ", nowDT, "ft.StartDT.Seconds ", ft.StartDT.Seconds())

		pubDuration := 0

		if ft.EndDT.Seconds() < me.startTime+me.duration*60 {
			//推流到文件结束
			log.Debug(PrintTaskId(me), "EndDT: ", ft.EndDT.Seconds(), "   end: ", me.startTime+me.duration*60)
			pubDuration = 0
		} else {
			//需要推流restDuration秒结束推流
			pubDuration = me.duration*60 - (nowDT - me.startTime) //剩余推流时长
			log.Debug(PrintTaskId(me), "pubDuration: ", pubDuration, me.duration*60, nowDT, me.startTime)
		}

		if restDuration > 0 {
			//剩余restDuration时间需要推流
			pub := new(Publisher)
			me.pub = pub

			log.Debug(PrintTaskId(me), "DoPublish: ", "offset ", offset, " restDuration",
				restDuration, " ", me.Host, " ", me.Vhost, " ", me.StreamName,
				" ", ft.Path)

			err := pub.DoPublish(offset, pubDuration, me.Host, me.Vhost, me.StreamName, ft.Path)
			if err != nil {
				log.Error("DoPublish error, republish", err)
				time.Sleep(1 * time.Second)
			}
		} else {
			log.Debug(PrintTaskId(me), "end task: ", me.taskId)
			me.pub = nil
			break
		}
	}
	//任务结束
	me.taskEndCh <- me.taskId
}

func PrintTaskId(task *Task) string {
	return fmt.Sprintf("[%d] ", task.taskId)
}
