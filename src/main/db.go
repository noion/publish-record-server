package main

import (
	"container/list"
	"database/sql"
	//"fmt"
	"os"

	log "github.com/cihub/seelog"
	_ "github.com/mattn/go-sqlite3"
)

type DBService interface {
	InitDB(string) error                                    //初始化数据库
	UpdateStreams(list.List) error                          //更新流列表
	GetStreams() (list.List, error)                         //获取流列表
	GetStreamId(vhost, hub, streamName string) (int, error) //获取流Id
	InsertFileTime(string, int, int, int) error             //插入文件路径和对应时间
	UpdateFileTime([]*FileTime) error

	GetFileTime(StreamId, t int) *FileTime //获取该时间的FileTime
	Close() error
}

type StreamInfo struct {
	Vhost      string `json:"vhost"`      //vhost
	StreamName string `json:"streamName"` //流名称
	Hub        string `json:"hub"`        //hub名称
	StreamId   int    `json:"streamId"`   //从中心服务器获取的流Id
	Host       string `json:"host"`       //rtmp流服务器IP:Port
}

type FileTime struct {
	FileId   int
	Path     string
	StreamId int
	StartDT  *DayTime
	EndDT    *DayTime
}

type SqliteDbService struct {
	db *sql.DB //数据库连接
}

//初始化数据库
func (me *SqliteDbService) InitDB(connString string) error {

	dbExist := false
	_, err := os.Stat(connString)
	if err == nil {
		dbExist = true
	}

	db, err := sql.Open("sqlite3", connString)
	if err != nil {
		log.Error(err)
		return err
	}

	//数据库不存在，新建数据库和表
	if !dbExist {
		sqlStmt := `create table prs_streams (stream_id integer not null primary key AUTOINCREMENT, vhost varchar(255), stream_name varchar(255), hub varchar(255));`
		_, err = db.Exec(sqlStmt)
		if err != nil {
			log.Errorf("%q: %s\n", err, sqlStmt)
			db.Close()
			return err
		}

		sqlStmt = "create table prs_file_time (file_id integer not null  primary key AUTOINCREMENT, stream_id integer, path varchar(255), start_time long, end_time long);"
		_, err = db.Exec(sqlStmt)
		if err != nil {
			log.Errorf("%q: %s\n", err, sqlStmt)
			db.Close()
			return err
		}
	}
	me.db = db
	return nil
}

func (me *SqliteDbService) UpdateStreams(streams list.List) error {
	tx, err := me.db.Begin()
	if err != nil {
		log.Error("UpdateStreams sql DB.Begin error: ", err)
		return err
	}

	defer func() {
		if err != nil {
			err = tx.Rollback()
			if err != nil {
				log.Error("UpdateStreams Rollback error", err)
			}
			return
		} else {
			err = tx.Commit()
			if err != nil {
				log.Error("UpdateStreams Commit error", err)
			}
		}
	}()

	_, err = tx.Exec("delete from prs_streams")
	if err != nil {
		log.Error("UpdateStreams delete error", err)
		return err
	}

	stmt, err := tx.Prepare("insert into prs_streams (vhost, stream_name, hub) values(?, ?, ?)")

	for e := streams.Front(); e != nil; e = e.Next() {
		info := e.Value.(StreamInfo)
		_, err = stmt.Exec(info.Vhost, info.StreamName, info.Hub)
		if err != nil {
			stmt.Close() //stmt需要在tx commit或者rollback之前关闭
			log.Error("UpdateStreams insert error", err)
			return err
		}
	}
	stmt.Close()
	return nil
}

func (me *SqliteDbService) GetStreamId(vhost, hub, streamName string) (int, error) {
	var streamId int
	err := me.db.QueryRow("select stream_id from prs_streams where stream_name == ? and hub == ? and vhost = ?",
		streamName, hub, vhost).Scan(&streamId)
	if err != nil {
		log.Error("GetStreamId error, ", err)
		return 0, err
	}
	return streamId, nil
}

//获取流列表
func (me *SqliteDbService) GetStreams() (list.List, error) {

	var l list.List

	rows, err := me.db.Query("select * from prs_streams")
	defer rows.Close()
	if err != nil {
		log.Error("GetStreams query error", err)
		return l, err
	}

	for rows.Next() {
		var info StreamInfo
		err := rows.Scan(&info.StreamId, &info.StreamName, &info.Hub)
		if err != nil {
			log.Error("GetStreams rows scan error", err)
			return l, err
		}
		l.PushBack(info)
	}
	return l, err
}

//插入文件时间，
func (me *SqliteDbService) InsertFileTime(path string, streamId, start, end int) error {
	_, err := me.db.Exec("insert into prs_file_time (path, stream_id, start_time, end_time) values(?,?,?,?)", path, streamId, start, end)
	if err != nil {
		log.Error("InsertFileTime error, ", err)
		return err
	}
	return nil
}

func (me *SqliteDbService) UpdateFileTime(fileTimes []*FileTime) error {
	tx, err := me.db.Begin()
	if err != nil {
		log.Error("UpdateFileTime sql DB.Begin error: ", err)
		return err
	}
	defer func() {
		if err != nil {
			err = tx.Rollback()
			if err != nil {
				log.Error("UpdateFileTime Rollback error", err)
			}
			return
		} else {
			err = tx.Commit()
			if err != nil {
				log.Error("UpdateFileTime Commit error", err)
			}
		}
	}()

	_, err = tx.Exec("delete from prs_file_time")
	if err != nil {
		log.Error("UpdateFileTime delete error", err)
		return err
	}

	stmt, err := tx.Prepare("insert into prs_file_time (path, stream_id, start_time, end_time) values(?,?,?,?)")

	for _, ft := range fileTimes {
		_, err = stmt.Exec(ft.Path, ft.StreamId, ft.StartDT.Seconds(), ft.EndDT.Seconds())
		if err != nil {
			stmt.Close() //stmt需要在tx commit或者rollback之前关闭
			log.Error("UpdateFileTime insert error", err)
			return err
		}
	}
	stmt.Close()
	return nil
}

//根据时间，从数据库查询在包含该时间的录像文件
func (me *SqliteDbService) GetFileTime(StreamId, t int) *FileTime {

	ft := new(FileTime)

	var start int
	var end int

	err := me.db.QueryRow("select * from prs_file_time where stream_id = ? and start_time <= ? and end_time > ?",
		StreamId, t, t).Scan(&ft.FileId, &ft.StreamId, &ft.Path, &start, &end)
	if err != nil {
		log.Error("getfiletime error, ", err)
		return nil
	}

	ft.StartDT = NewDayTimeBySeconds(start)
	ft.EndDT = NewDayTimeBySeconds(end)

	log.Debug("GetFileTime: ", ft.FileId)
	return ft
}

func (me *SqliteDbService) Close() error {
	if me.db != nil {
		return me.db.Close()
	}
	return nil
}
