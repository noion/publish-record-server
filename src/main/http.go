package main

import (
	"html/template"
	"net/http"

	log "github.com/cihub/seelog"
)

type HttpService struct {
	task     *TaskManager
	httpPort int
	host     string
}

type PageData struct {
	Host string
}

func NewHttpService(task *TaskManager, httpPort int, host string) *HttpService {
	return &HttpService{task: task, httpPort: httpPort, host: host}
}

//处理根目录Get请求
func (me *HttpService) HandlerRoot(w http.ResponseWriter, req *http.Request) {
	tmpl := template.Must(template.ParseFiles("./tmpl/index.tmpl"))

	pageData := PageData{me.host}
	log.Debug(pageData)

	err := tmpl.Execute(w, pageData)
	if err != nil {
		log.Error("template execution: %s", err)
	}
}

func (me *HttpService) HandlerTest(w http.ResponseWriter, req *http.Request) {
	log.Debug("GET /test")

	me.task.PushTask("__defaultVhost__", "live", "test_record", "192.168.199.182:1935")
	w.WriteHeader(200)
}

func (me *HttpService) HandlerAddStream(w http.ResponseWriter, r *http.Request) {
	log.Debug("HandlerAddStream")

	if r.Method == "POST" {
		r.ParseForm()
		vhost := r.FormValue("vhost")
		if vhost == "" {
			w.Write([]byte("vhost 不能为空"))
			return
		}
		hub := r.FormValue("hub")
		if hub == "" {
			w.Write([]byte("hub 不能为空"))
			return
		}
		stream := r.FormValue("stream")
		if stream == "" {
			w.Write([]byte("stream 不能为空"))
			return
		}

		me.task.PushTask(vhost, hub, stream, me.host)
	}
}
