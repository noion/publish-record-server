package main

import (
	"time"
)

//表示一天里的相对时间，
type DayTime struct {
	H int //时
	M int //分
	S int //秒
}

func NewDayTime(H, M, S int) *DayTime {
	return &DayTime{H: H, M: M, S: S}
}

func NewDayTimeBySeconds(seconds int) *DayTime {
	H := seconds / 3600
	rest := seconds % 3600
	M := rest / 60
	rest = rest % 60
	S := rest
	return &DayTime{H: H, M: M, S: S}
}

//转换为总秒数
func (me *DayTime) Seconds() int {
	return me.H*60*60 + me.M*60 + me.S
}

//加上日期
func (me *DayTime) AddDate(year, month, day int) time.Time {
	return time.Date(year, time.Month(month), day, me.H, me.M, me.S, 0, time.UTC)
}

//从time.Time获取DayTime的值
func GetDayTimeSeconds(t time.Time) int {
	dt := NewDayTime(t.Hour(), t.Minute(), t.Second())
	return dt.Seconds()
}
