package main

import (
	"encoding/json"

	log "github.com/cihub/seelog"
)

type RmqBinder struct {
	manager *TaskManager
}

func NewRmqBinder(manager *TaskManager) *RmqBinder {
	return &RmqBinder{manager: manager}
}

func (me *RmqBinder) BindTaskListener(conn *MqConnection) error {
	createQueue := "publish.record.server.task"
	createKey := "publish.record.server.rkey"
	err := conn.BindListener(createQueue, createKey, me.onRecieve)
	if err != nil {
		log.Error("监听创建任务队列失败: ", err.Error())
		return err
	}
	return nil
}

type StreamParam struct {
	Vhost      string `json:"vhost"`      //vhost
	StreamName string `json:"streamName"` //流名称
	Hub        string `json:"hub"`        //hub名称
	Host       string `json:"host"`       //rtmp流服务器IP:Port
}

//接收消息
func (me *RmqBinder) onRecieve(data []byte) {
	log.Debug(string(data))
	var sp StreamParam
	if err := json.Unmarshal(data, &sp); err != nil {
		log.Error("解析任务失败: ", err.Error())
		return
	}

	me.manager.PushTask(sp.Vhost, sp.Hub, sp.StreamName, sp.Host)
}
