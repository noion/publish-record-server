package main

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"

	log "github.com/cihub/seelog"
)

//推流结构体
type Publisher struct {
	cmd *exec.Cmd
}

func (me *Publisher) DoPublish(offset, duraiton int, host, vhost, streamName, filePath string) error {

	log.Debug("DoPublish")

	output := "rtmp://" + host + "/live?vhost=" + vhost + "/" + streamName

	offsetString := strconv.Itoa(offset)
	duraitonString := strconv.Itoa(duraiton)

	var cmd *exec.Cmd
	if offset > 0 && duraiton > 0 {
		//ffmpeg 的 -ss 参数设置时间偏移
		//ffmpg的 -t 参数设置推流时间长度
		cmd = exec.Command("ffmpeg", "-re", "-ss", offsetString, "-i", filePath, "-vcodec",
			"copy", "-acodec", "copy", "-f", "flv", "-y", "-t", duraitonString, output)

		debug := fmt.Sprintf("cmd: ffmpeg -re -ss %s -i %s -vcodec copy -acodec copy -f flv -y -t %s %s",
			offsetString, filePath, duraitonString, output)
		log.Debug(debug)

	} else if offset > 0 {
		cmd = exec.Command("ffmpeg", "-re", "-ss", offsetString, "-i", filePath, "-vcodec",
			"copy", "-acodec", "copy", "-f", "flv", "-y", output)
		debug := fmt.Sprintf("cmd: ffmpeg -re -ss %s -i %s -vcodec copy -acodec copy -f flv -y %s",
			offsetString, filePath, output)
		log.Debug(debug)

	} else if duraiton > 0 {
		cmd = exec.Command("ffmpeg", "-re", "-i", filePath, "-vcodec",
			"copy", "-acodec", "copy", "-f", "flv", "-y", "-t", duraitonString, output)

		debug := fmt.Sprintf("cmd: ffmpeg -re -i %s -vcodec copy -acodec copy -f flv -y -t %s %s",
			filePath, duraitonString, output)
		log.Debug(debug)

	} else {
		cmd = exec.Command("ffmpeg", "-re", "-i", filePath, "-vcodec",
			"copy", "-acodec", "copy", "-f", "flv", "-y", output)

		debug := fmt.Sprintf("cmd: ffmpeg -re -i %s -vcodec copy -acodec copy -f flv -y %s",
			filePath, output)
		log.Debug(debug)
	}

	me.cmd = cmd

	logFileName := "ffmpeg-encoder-" + vhost + "-live-" + streamName + ".log"
	file, err := os.OpenFile(logFileName, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0x777)
	defer file.Close()
	if err != nil {
		log.Error("OpenFile failded", err)
		return err
	}

	cmd.Stderr = file

	if err := cmd.Start(); err != nil {
		return err
	}

	err = cmd.Wait()
	if err != nil {
		log.Error("cmd Wait error, ", err)
		return err
	}

	log.Debug("exit publish")
	return nil
}

func (me *Publisher) Kill() error {
	if me.cmd != nil {
		return me.cmd.Process.Kill()
	}
	return nil
}
