package main

import (
	"errors"
	"time"

	log "github.com/cihub/seelog"

	"github.com/pborman/uuid"
	"github.com/streadway/amqp"
)

type MqConnection struct {
	ampqurl         string
	conn            *amqp.Connection
	closeReceiver   chan *amqp.Error
	sendChan        chan *sendParam
	mqPulishChannel *amqp.Channel
	mqBindChannels  map[string]*amqp.Channel
	mqSendChannel   *amqp.Channel
	disconnHandler  DisconnectedHandler
}

type sendParam struct {
	Exchange string
	Key      string
	msg      []byte
}

type DisconnectedHandler func()

func NewMqConnection(ampq string, handler DisconnectedHandler) (*MqConnection, error) {
	return &MqConnection{
		ampqurl:        ampq,
		disconnHandler: handler,
		mqBindChannels: make(map[string]*amqp.Channel),
		sendChan:       make(chan *sendParam, 100),
		conn:           nil}, nil
}

func (m *MqConnection) Connect() error {
	conn, err := amqp.Dial(m.ampqurl)
	if err != nil {
		return err
	}
	m.conn = conn
	m.mqPulishChannel, err = m.conn.Channel()
	if err != nil {
		return err
	}
	m.closeReceiver = make(chan *amqp.Error)
	m.conn.NotifyClose(m.closeReceiver)
	go m.closedWait()
	return nil
}

func (m *MqConnection) closedWait() {
WAIT:
	err := <-m.closeReceiver
	if err != nil {
		//非正常关闭
		for {
			log.Debug("rabbitmq reconnecting...")
			err := m.Connect()
			if err == nil {
				log.Debug("rabbitmq reconnected")
				m.mqBindChannels = make(map[string]*amqp.Channel)
				m.mqPulishChannel, _ = m.conn.Channel()
				m.disconnHandler()
				goto WAIT
			}
			time.Sleep(time.Second * 5)
		}
	}
}

func (m *MqConnection) CloseConnection() error {
	for _, ch := range m.mqBindChannels {
		ch.Close()
	}
	m.mqPulishChannel.Close()
	return m.conn.Close()
}

type MqReceiveHandler func(data []byte)

//创建一个已经绑定队列的channel
func (m *MqConnection) BindListener(qname string, rkey string, listner MqReceiveHandler) error {
	mqCh, err := m.conn.Channel()
	if err != nil {
		return err
	}

	if _, ok := m.mqBindChannels[qname]; ok {
		return errors.New(qname + "queue have binded")
	}

	m.mqBindChannels[qname] = mqCh

	//声明队列
	_, err = mqCh.QueueDeclare(
		qname, // name
		true,  // durable
		false, // delete when usused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		return err
	}
	//绑定rkey
	err = mqCh.QueueBind(
		qname,        // queue name
		rkey,         // routing key
		"amq.direct", // exchange
		false,
		nil)
	if err != nil {
		return err
	}

	msgs, err := mqCh.Consume(
		qname, // queue
		qname, // consumer
		true,  // auto ack
		false, // exclusive
		false, // no local
		false, // no wait
		nil,   // args
	)
	if err != nil {
		return err
	}
	go consumeCycle(listner, msgs)
	return nil
}

func (m *MqConnection) CancelConsume(qname string) error {
	ch, ok := m.mqBindChannels[qname]
	if !ok {
		return errors.New(qname + "queue not bind")
	}
	return ch.Cancel(qname, true)
}

func consumeCycle(listner MqReceiveHandler, msgs <-chan amqp.Delivery) {
	for d := range msgs {
		listner(d.Body)
	}
}

//接收超时时间
var recv_timeout = 30

func (m *MqConnection) SetTimeout(timeout int) {
	recv_timeout = timeout
}

//由于rabbitmq的同一个channel不能用于多线程，为了避免获取过多的channle，
//所以这里用一个专门的channel处理发送消息，并且在一条线程中执行
func (m *MqConnection) Send(exchange, key string, msg []byte) {
	param := &sendParam{exchange, key, msg}
	m.sendChan <- param
}

func (m *MqConnection) sendCycle(sendChan chan *sendParam) {
	for {
		param, ok := <-sendChan
		if !ok {
			return
		}

		p := amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         param.msg,
		}
		err := m.mqSendChannel.Publish(param.Exchange, param.Key, false, false, p)
		if err != nil {
			log.Info("Publish error: ", err.Error())
		}
	}
}

//发送消息阻塞接收回复消息
func (m *MqConnection) ConvertSendAndReceive(rkey string, msg []byte) ([]byte, error) {
	q, err := m.mqPulishChannel.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when usused
		true,  // exclusive
		false, // noWait
		nil,   // arguments
	)
	if err != nil {
		return nil, err
	}

	err = m.mqPulishChannel.Publish(
		"amq.direct", // exchange
		rkey,         // routing key
		false,        // mandatory
		false,        // immediate
		amqp.Publishing{
			ContentType:   "application/json",
			CorrelationId: uuid.New(),
			ReplyTo:       q.Name,
			Body:          msg,
		})
	if err != nil {
		return nil, err
	}

	msgs, err := m.mqPulishChannel.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto ack
		false,  // exclusive
		false,  // no local
		false,  // no wait
		nil,    // args
	)

	n := recv_timeout * 1000 / 500
	for i := 0; i < n; i++ {
		select {
		case d := <-msgs:
			return d.Body, nil
		case <-time.After(time.Millisecond * 500):
			continue
		}
	}
	return nil, errors.New("wait response timeout")
}
