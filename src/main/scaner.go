package main

import (
	"errors"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	log "github.com/cihub/seelog"
)

type Scaner struct {
	db        DBService
	fileTimes []*FileTime
	root      string
}

func NewScanner(db *DBService) *Scaner {
	return &Scaner{db: *db}
}

//遍历根目录文件，将文件对应时间解析出来，保存到数据库
func (me *Scaner) ScanFile(dir string) error {
	log.Debug("ScanFile")
	me.root = dir

	//遍历目录
	if err := filepath.Walk(dir, me.walkFunc); err != nil {
		log.Error("filepath Walk error,", err)
		return err
	}

	me.db.UpdateFileTime(me.fileTimes)
	return nil
}

func (me *Scaner) walkFunc(filepath string, osfi os.FileInfo, err error) error {

	streamDir, _ := path.Split(filepath)

	if !osfi.IsDir() && (path.Clean(path.Dir(path.Dir(streamDir))) == me.root) {
		//确定是root子文件夹下的文件

		dirName := path.Base(streamDir)
		streamId, err := strconv.Atoi(dirName)
		if err != nil {
			return errors.New("strconv: " + err.Error())
		}

		startTime, endTime, err := me.marshalFileName(osfi.Name())
		if err != nil {
			return errors.New("marshalFileName: " + err.Error())
		}
		info := &FileTime{Path: filepath, StreamId: streamId,
			StartDT: NewDayTimeBySeconds(startTime), EndDT: NewDayTimeBySeconds(endTime)}

		me.fileTimes = append(me.fileTimes, info)

		log.Debug("Pushback: ", info.Path, info.StreamId, info.StartDT, info.EndDT)
	}
	return nil
}

//从文件名解析文件播放的开始时间和结束时间
//例如： 0-0-0_1-0-0_test.mp4 解析开始时间为从0点0分0秒 结束时间为1点0分0秒
func (me *Scaner) marshalFileName(name string) (int, int, error) {
	s := strings.Split(name, "_")
	if len(s) < 2 {
		return 0, 0, errors.New("len(s) < 2: " + name)
	}

	sub1 := s[0]
	sub2 := s[1]

	stars := strings.Split(sub1, "-")
	ends := strings.Split(sub2, "-")

	if (len(stars) != 3) || (len(ends) != 3) {
		return 0, 0, errors.New("开始或者结束时间无法解析: " + name)
	}

	startH, err := strconv.Atoi(stars[0])
	if err != nil {
		return 0, 0, errors.New("文件名无法解析开始时间-时: " + name)
	}
	startM, err := strconv.Atoi(stars[1])
	if err != nil {
		return 0, 0, errors.New("文件名无法解析开始时间-分: " + name)
	}
	startS, err := strconv.Atoi(stars[2])
	if err != nil {
		return 0, 0, errors.New("文件名无法解析开始时间-秒: " + name)
	}

	endH, err := strconv.Atoi(ends[0])
	if err != nil {
		return 0, 0, errors.New("文件名无法解析结束时间-时: " + name)
	}
	endM, err := strconv.Atoi(ends[1])
	if err != nil {
		return 0, 0, errors.New("文件名无法解析结束时间-分: " + name)
	}
	endS, err := strconv.Atoi(ends[2])
	if err != nil {
		return 0, 0, errors.New("文件名无法解析结束时间-秒: " + name)
	}

	startTime := startH*60*60 + startM*60 + startS
	endTime := endH*60*60 + endM*60 + endS
	return startTime, endTime, nil
}
