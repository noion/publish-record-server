package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	log "github.com/cihub/seelog"
	"gopkg.in/yaml.v2"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config", "../conf/record-server.config", "path for config")
}

func main() {

	flag.Parse()

	config, err := ParserConfig(configPath)
	if err != nil {
		fmt.Println(fmt.Sprintf("加载配置文件失败: %s,请确认路径是否正确：%s", err.Error(), configPath))
		return
	}

	//初始化日志
	logger, err := log.LoggerFromConfigAsFile(config.LogConfig)
	if err != nil {
		fmt.Println(fmt.Sprintf("加载日志配置失败,请确认路径是否正确：%s", config.LogConfig))
		return
	}
	log.ReplaceLogger(logger)

	//初始化数据库
	sqliteDb := new(SqliteDbService)
	sqliteDb.InitDB(config.SqlUrl)

	//初始化文件扫描
	var db DBService = sqliteDb
	scanner := NewScanner(&db)
	scanner.ScanFile(config.Root)

	//启动任务管理
	manager := NewTaskManager(&db, config.Duration)
	manager.Start()

	//连接rabbitmq
	rmq, err := NewMqConnection(config.Amqp, RmqDisconnectedHandler)
	if err != nil {
		return
	}

	for {
		if err := rmq.Connect(); err != nil {
			log.Error("连接rabbitmq失败, 重新连接...", err.Error())
			time.Sleep(time.Second * 5)
			continue
		}
		break
	}

	//绑定队列
	rmqListener := NewRmqBinder(manager)
	rmqListener.BindTaskListener(rmq)

	//启动httpserver
	httpService := NewHttpService(manager, config.HttpPort, config.RtmpHost)

	http.HandleFunc("/", httpService.HandlerRoot)
	http.HandleFunc("/test", httpService.HandlerTest)
	addr := config.HttpIp + ":" + strconv.Itoa(config.HttpPort)

	err = http.ListenAndServe(addr, nil)
	if err != nil {
		log.Error("ListenAndServe error: ", err)
	}

	log.Info("Server exit.")
}

//rabbitmq掉线重连处理
func RmqDisconnectedHandler() {

}

//配置
type Config struct {
	LogConfig  string `yaml:"LogConfig"`  //日志配置文件路径
	SqlUrl     string `yaml:"SqlUrl"`     //数据库连接或者sqlite路径
	Amqp       string `yaml:"amqp"`       //rabbitmq连接
	HttpIp     string `yaml:"HttpIp"`     //httpAPI监听IP
	HttpPort   int    `yaml:"HttpPort"`   //httpAPI监听端口
	RtmpHost   string `yaml:"RtmpHost"`   //RTMP流服务器 IP:Port
	Root       string `yaml:"Root"`       //推流录像文件根目录
	Duration   int    `yaml:"Duration"`   //单位分钟
	FfprobeBin string `yaml:"ffprobeBin"` //ffprobe bin 路径
}

func ParserConfig(path string) (*Config, error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, errors.New("read config failed")
	}

	var config Config
	err = yaml.Unmarshal(bytes, &config)
	if err != nil {
		return nil, errors.New("yaml unmarshal failed,")
	}

	fmt.Printf("全局配置 LogConfig: %s , SqlUrl: %s\n", config.LogConfig, config.SqlUrl)
	return &config, nil
}
